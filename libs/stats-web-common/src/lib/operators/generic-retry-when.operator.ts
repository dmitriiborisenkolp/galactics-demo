import { Observable, throwError, timer } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

export const DEFAULT_RETRY_REQUEST_DURATION = 1000;

export const genericRetryStrategy =
  (
    {
      retryDuration = DEFAULT_RETRY_REQUEST_DURATION,
      excludedStatusCodes = [],
    }: {
      retryDuration?: number;
      excludedStatusCodes?: Array<number>;
    } = {
      retryDuration: DEFAULT_RETRY_REQUEST_DURATION,
    },
  ) =>
  (attempts: Observable<any>) => {
    let countAttempts = 0;

    return attempts.pipe(
      mergeMap((error) => {
        if (!(!!error && 'status' in error && error.status > 0)) {
          countAttempts++;
        }

        if (!!error && error.status) {
          if (!excludedStatusCodes.find((e) => e === error.status)) {
            return throwError(error);
          }
        }

        return timer(retryDuration);
      }),
    );
  };
