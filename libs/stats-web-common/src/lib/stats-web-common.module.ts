import { ModuleWithProviders, NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { StatsWebHttpService } from './services/stats-web-http.service';
import { STATS_WEB_COMMON_MODULE_CONFIG, StatsWebCommonModuleConfig } from './stats-web-common.config';
import { StatsWebLoadingService } from './services/stats-web-loading.service';

@NgModule({
  imports: [HttpClientModule],
  providers: [StatsWebHttpService, StatsWebLoadingService],
})
export class StatsWebCommonModule {
  static forRoot(config: StatsWebCommonModuleConfig): ModuleWithProviders<StatsWebCommonModule> {
    return {
      ngModule: StatsWebCommonModule,
      providers: [
        {
          provide: STATS_WEB_COMMON_MODULE_CONFIG,
          useValue: config,
        },
      ],
    };
  }
}
