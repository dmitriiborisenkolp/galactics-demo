import { Inject, Injectable } from '@angular/core';
import { STATS_WEB_COMMON_MODULE_CONFIG, StatsWebCommonModuleConfig } from '../stats-web-common.config';
import { catchError, Observable, throwError } from 'rxjs';
import { withoutTrailingSlash } from '../utils/without-trailing-slash.util';
import { HttpClient, HttpErrorResponse, HttpParams } from '@angular/common/http';

type Params =
  | HttpParams
  | {
      [param: string]: string | number | boolean | ReadonlyArray<string | number | boolean>;
    };

export const ngError = () => (input: Observable<any>) =>
  input.pipe(catchError((err: HttpErrorResponse) => throwError(!!err && typeof err === 'object' && err.error ? err.error : err)));

@Injectable()
export class StatsWebHttpService {
  constructor(@Inject(STATS_WEB_COMMON_MODULE_CONFIG) private readonly config: StatsWebCommonModuleConfig, private readonly http: HttpClient) {}

  get apiBaseUrl(): string {
    return this.config.statsApiBaseUrl;
  }

  url(endpoint: any): string {
    return `${this.apiBaseUrl}/${withoutTrailingSlash(endpoint as any)}`;
  }

  path(path: string, args: Record<string, unknown>): string {
    let interpolated = path;

    for (const key of Object.keys(args)) {
      while (interpolated.includes(`/:${key}/`)) {
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        interpolated = interpolated.replace(`/:${key}/`, args[key]);
      }

      while (interpolated.endsWith(`/:${key}`)) {
        interpolated = `${interpolated.slice(0, interpolated.length - key.length - 2)}/${args[key]}`;
      }
    }

    return interpolated;
  }

  get<R, T = any>(endpoint: T, request?: Params): Observable<R> {
    return this.http
      .get<R>(this.url(endpoint), {
        params: request,
      })
      .pipe(ngError());
  }

  post<R, T = any>(endpoint: T, request: Params = {}, options?: any): Observable<R> {
    return this.http.post<R>(this.url(endpoint), request, options).pipe(ngError());
  }

  patch<R, T = any>(endpoint: T, request: Params = {}): Observable<R> {
    return this.http.patch<R>(this.url(endpoint), request).pipe(ngError());
  }

  put<R, T = any>(endpoint: T, request: Params): Observable<R> {
    return this.http.put<R>(this.url(endpoint), request).pipe(ngError());
  }

  head<R, T = any>(endpoint: T): Observable<R> {
    return this.http.head<R>(this.url(endpoint)).pipe(ngError());
  }

  delete<R, T = any>(endpoint: T): Observable<R> {
    return this.http.delete<R>(this.url(endpoint)).pipe(ngError());
  }
}
