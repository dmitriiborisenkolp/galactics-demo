export interface StatsWebCommonModuleConfig {
  /**
   * Base URL for Stats API (JAR)
   */
  statsApiBaseUrl: string;
}

export const STATS_WEB_COMMON_MODULE_CONFIG = Symbol();
