export interface StatsWebCommonEnvironment {
    /**
     * Enable / Disable Production mode for Angular
     */
    production: boolean;

    /**
     * Base URL for Stats API (JAR)
     */
    statsApiBaseUrl: string;
}
