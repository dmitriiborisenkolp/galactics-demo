export interface StatsApiProcessDto {
    /** @example "2023-06-22T13:45:17.256973384Z: ok" */
    status: string;
}
