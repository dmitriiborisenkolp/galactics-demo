export interface StatsApiParamsDto {
    /** @example: "5" */
    count: string;

    /** @example "1000" */
    delay: string;
}
