import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { StatsApiParamsDto } from '../dtos/stats-api-params.dto';
import { StatsWebHttpService } from '@galactics-demo/stats-web-common';
import { StatsApiProcessDto } from '../dtos/stats-api-process.dto';

@Injectable()
export class StatsApiDataAccess {
  constructor(private readonly http: StatsWebHttpService) {}

  params(): Observable<StatsApiParamsDto> {
    return this.http.post('/', { action: 'params' });
  }

  process(): Observable<StatsApiProcessDto> {
    return this.http.post('/', { action: 'process' });
  }
}
