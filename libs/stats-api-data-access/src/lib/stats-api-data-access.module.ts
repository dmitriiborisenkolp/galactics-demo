import { NgModule } from '@angular/core';
import { StatsWebCommonModule } from '@galactics-demo/stats-web-common';
import { StatsApiDataAccess } from './services/stats-api.data-access';

@NgModule({
  imports: [StatsWebCommonModule],
  providers: [StatsApiDataAccess],
})
export class StatsApiDataAccessModule {}
