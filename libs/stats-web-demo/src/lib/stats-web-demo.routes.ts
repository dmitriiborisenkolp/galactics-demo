import { Routes } from '@angular/router';
import { StatsWebDemoComponent } from './components/stats-web-demo/stats-web-demo.component';

export const StatsWebDemoRoutes: Routes = [
  {
    path: '',
    component: StatsWebDemoComponent,
  },
];
