import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { StatsWebDemoRoutes } from './stats-web-demo.routes';

@NgModule({
  imports: [RouterModule.forChild(StatsWebDemoRoutes)],
  exports: [RouterModule],
})
export class StatsWebDemoRoutingModule {}
