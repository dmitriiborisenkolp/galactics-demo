import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StatsApiDataAccessModule } from '@galactics-demo/stats-api-data-access';
import { StatsWebDemoComponent } from './components/stats-web-demo/stats-web-demo.component';
import { StatsWebDemoRoutingModule } from './stats-web-demo.routing-module';
import { StatsWebCommonModule } from '@galactics-demo/stats-web-common';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatCardModule } from '@angular/material/card';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatButtonModule } from '@angular/material/button';
import { MatTableModule } from '@angular/material/table';

@NgModule({
  imports: [
    CommonModule,
    StatsWebDemoRoutingModule,
    StatsApiDataAccessModule,
    StatsWebCommonModule,
    MatSnackBarModule,
    MatCardModule,
    MatProgressBarModule,
    MatButtonModule,
    MatTableModule,
  ],
  declarations: [StatsWebDemoComponent],
})
export class StatsWebDemoModule {}
