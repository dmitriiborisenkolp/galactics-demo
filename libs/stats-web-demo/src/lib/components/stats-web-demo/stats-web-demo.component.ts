import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { StatsApiDataAccess, StatsApiParamsDto, StatsApiProcessDto } from '@galactics-demo/stats-api-data-access';
import { finalize, timer, Observable, retryWhen, shareReplay, startWith, Subject, switchMap, take, takeUntil, map, zip, tap, of } from 'rxjs';
import { genericRetryStrategy, statsWebCommonConstants, StatsWebLoadingService } from '@galactics-demo/stats-web-common';
import { MatSnackBar } from '@angular/material/snack-bar';
import * as dayjs from 'dayjs';

interface State {
  params?: StatsApiParamsDto;
  entries?: ResponseEntry[];
}

interface ResponseEntry {
  date: string;
  response: StatsApiProcessDto;
}

@Component({
  templateUrl: './stats-web-demo.component.html',
  styleUrls: ['./stats-web-demo.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class StatsWebDemoComponent implements OnInit, OnDestroy {
  private readonly ngOnDestroy$ = new Subject<void>();
  private readonly nextProcess$ = new Subject<void>();

  state: State = {};
  displayedColumns = ['date', 'response'];

  constructor(
    private readonly cdr: ChangeDetectorRef,
    private readonly dataAccess: StatsApiDataAccess,
    private readonly loading: StatsWebLoadingService,
    private readonly snackbar: MatSnackBar,
  ) {}

  ngOnInit(): void {
    this.fetchParams();
  }

  ngOnDestroy(): void {
    this.nextProcess$.next(undefined);
    this.ngOnDestroy$.next(undefined);
  }

  get isLoading$(): Observable<boolean> {
    return this.loading.isLoading$;
  }

  fetchParams(): void {
    const loading = this.loading.addLoading();

    this.dataAccess
      .params()
      .pipe(
        retryWhen(genericRetryStrategy()),
        finalize(() => loading.complete()),
        takeUntil(this.ngOnDestroy$),
      )
      .subscribe({
        next: (params) => {
          this.state = {
            ...this.state,
            params,
          };

          this.snackbar.open(`Params fetched`, undefined, {
            duration: statsWebCommonConstants.matSnackBarDuration,
          });

          this.cdr.markForCheck();
        },
        error: (err) => {
          this.snackbar.open(`Failed to fetch Params: ${err}`, undefined, {
            duration: statsWebCommonConstants.matSnackBarDuration,
          });
        },
      });
  }

  process(): void {
    this.snackbar.dismiss();
    this.nextProcess$.next(undefined);

    if (!this.state.params) {
      this.snackbar.open('No params available');

      return;
    }

    const { delay, count } = this.state.params;

    const observables: Observable<ResponseEntry>[] = [];

    for (let i = 0; i < +count; i++) {
      observables.push(
        timer(i * +delay).pipe(
          tap(() => {
            this.snackbar.dismiss();
            this.snackbar.open(`Sending #${i + 1} request`);
          }),
          switchMap(() => zip(of(new Date()), this.dataAccess.process())),
          map(([date, response]) => ({ date: dayjs(date).format('hh:mm:ss:SSS'), response })),
        ),
      );
    }

    const loading = this.loading.addLoading();

    zip(observables)
      .pipe(
        finalize(() => loading.complete()),
        takeUntil(this.nextProcess$),
      )
      .subscribe({
        next: (entries: ResponseEntry[]) => {
          this.state = { ...this.state, entries };

          this.cdr.markForCheck();

          this.snackbar.dismiss();
          this.snackbar.open('All requests processed', undefined, {
            duration: statsWebCommonConstants.matSnackBarDuration,
          });
        },
      });
  }
}
