# GalacticsDemo

This demo is built with [Nx.Dev](https://nx.dev/) Monorepo system. This monorepo is also configured with `husky` + `commitlint`, `prettier` and `lint-staged` tools.

## How to run project on local environment

1. Build & Run API (see "How to start API" section)
2. Install NPM Packages

```shell
yarn
```

3. Run `serve` for Web application (with `proxy.conf.js`):

```shell
yarn serve
```

## How to start API

```shell
docker build -t galactics-stats-api -f stats.api.Dockerfile .
docker run -p 4523:4523 --name galactics-stats-api galactics-stats-api
```

API will be accessible within [http://localhost:4523]() URL. Example response:

```json
{ "status": "2023-06-22T13:03:52.434530845Z: error" }
```

## How to start Web

```shell
docker build -t galactics-stats-web -f stats.web.Dockerfile .
docker run -p 8080:80 --name galactics-stats-web galactics-stats-web
```

Web application will be accessible within [http://localhost:8080]() URL
