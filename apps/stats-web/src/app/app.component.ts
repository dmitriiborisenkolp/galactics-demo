import { Component } from '@angular/core';

@Component({
  selector: 'galactics-demo-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less'],
})
export class AppComponent {
  title = 'stats-web';
}
