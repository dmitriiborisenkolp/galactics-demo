import { Routes } from '@angular/router';

export const appRoutes: Routes = [
  {
    path: '',
    loadChildren: () => import('@galactics-demo/stats-web-demo').then((m) => m.StatsWebDemoModule),
  },
];
