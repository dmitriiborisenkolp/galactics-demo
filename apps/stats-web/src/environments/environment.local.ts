import { StatsWebCommonEnvironment } from '@galactics-demo/stats-web-common';

export const environment: StatsWebCommonEnvironment = {
  statsApiBaseUrl: 'http://localhost:4200/api',
  production: false,
};
