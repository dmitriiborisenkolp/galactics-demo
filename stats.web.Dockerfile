FROM node:16.16.0-alpine3.16 AS build
WORKDIR /dist/src/app

RUN apk add g++ make py3-pip

COPY . .

RUN yarn install --frozen-lockfile
RUN yarn build

FROM nginx:latest AS ngi
COPY --from=build /dist/src/app/dist/apps/stats-web /usr/share/nginx/html
COPY ops/stats-web/nginx.conf  /etc/nginx/conf.d/default.conf

EXPOSE 80
