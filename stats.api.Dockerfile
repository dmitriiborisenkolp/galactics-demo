FROM amazoncorretto:20-alpine3.14
COPY apps/stats-api/bin/server-test.jar /home/server-test.jar
CMD ["java","-jar","/home/server-test.jar"]
